#include "Wire.h"
#include "BlinkM_funcs.h"

const int pingPin = 7;
byte blinkm_addr = 0x09;
const int pirPin = 2;
const int ledPin = 13;
int calibrationTime = 10;        
long unsigned int lowIn;
long unsigned int pause = 5000; 
long unsigned int motionEndedAt; 

boolean lockLow = true;
boolean takeLowTime;  

void setup() {

  BlinkM_beginWithPower();
  BlinkM_setAddress(blinkm_addr);
  BlinkM_stopScript(blinkm_addr);
  BlinkM_fadeToRGB(blinkm_addr, 0x00, 0x00, 0x00);

  pinMode(ledPin, OUTPUT);
  pinMode(pirPin, INPUT);
  digitalWrite(pirPin, LOW);

  for(int i = 0; i < calibrationTime; i++){
    blinkLed();
    delay(1000);
  }

  delay(50);
}

void blinkLed() {
  digitalWrite(ledPin, HIGH);   
  delay(100);   
  digitalWrite(ledPin, LOW);   
  delay(100);
}

void loop()
{
  long duration, inches, cm;

  if(digitalRead(pirPin) == HIGH){
    if(lockLow){  
      lockLow = false;            
      delay(50);
    }         
    takeLowTime = true;
  }

  if(digitalRead(pirPin) == LOW){       
    if(takeLowTime){
      lowIn = millis();          
      takeLowTime = false;
    }

    if(!lockLow && millis() - lowIn > pause){  
      lockLow = true;                        
      delay(50);
      motionEndedAt = millis();
    }
  }
  if (!lockLow || (millis() - motionEndedAt) < 20000) {

    pinMode(pingPin, OUTPUT);
    digitalWrite(pingPin, LOW);
    delayMicroseconds(2);
    digitalWrite(pingPin, HIGH);
    delayMicroseconds(5);
    digitalWrite(pingPin, LOW);

    pinMode(pingPin, INPUT);
    duration = pulseIn(pingPin, HIGH);

    inches = microsecondsToInches(duration);

    if (inches > 10) {
      BlinkM_fadeToRGB(blinkm_addr, 0x00, 0x00, 0x00);
    }

    if (inches > 6 && inches <= 10) {
      BlinkM_fadeToRGB(blinkm_addr, 0x00, 0xff, 0x00);
    }

    if (inches > 2 && inches <= 6) {
      BlinkM_fadeToRGB(blinkm_addr, 0xff, 0xff, 0x00);
    }

    if (inches > 0 && inches <= 2) {
      BlinkM_fadeToRGB(blinkm_addr, 0xff, 0x00, 0x00);
    }

    delay(100);
  }
}

long microsecondsToInches(long microseconds)
{
  return microseconds / 74 / 2;
}

